# ContextNet Dependencies #

This repo is a Maven repository for ContextNet dependencies.

### Usage ###

Inside the **pom.xml** file add the dependency and repository like this.

```
#!xml
<dependencies>
    <dependency>
        <groupId>br.pucrio.inf.lac</groupId>
    	<artifactId>contextnet</artifactId>
    	<version>2.5</version>
    </dependency>
</dependencies>

<repositories>
    <repository>
        <id>LAC PUC-Rio</id>
        <url>https://bitbucket.org/endler/contextnet-dependencies/raw/master</url>
    </repository>
</repositories>
```
Obs.: Remember to change the <version> if you are using a newer version.